﻿var famsfunc = {
    init: function (weburl, weburl2) {
        this.weburl = weburl;
        this.weburl2 = weburl2;
    },
    sleep: function (milliseconds) {
        var start = new Date().getTime();
        for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds) {
                break;
            }
        }
    },
    deletefromtableNew: function (dgIndex, dgData, sp) {
        return new Promise(function (resolve, reject) {
            $.messager.confirm('Confirm', 'Are you sure?', function (r) {
                if (r) {
                    $.ajax
                        ({
                            type: "GET",
                            async: false,
                            url: weburl2 + 'queryType=SqlSp&sp=' + sp + '&paramlist=' + dgData.rows[dgIndex].ID + ',' + sessionStorage.getItem("User_ID"),
                            success: function (html) {
                                if (html.indexOf('Failed') !== -1) {
                                    $.messager.alert('Failed', 'Unable to remove', function (r2) { });
                                    reject();
                                }
                                else {

                                    $.messager.alert('Success', 'Successfully removed', function (r2) {

                                    });
                                    resolve();
                                }
                            }
                        });
                }
            });
        });
    },
    deletefromtable: function (dgIndex, dgData, sp) {
        return new Promise(function (resolve, reject) {
            $.messager.confirm('Confirm', 'Are you sure?', function (r) {
                if (r) {
                    $.ajax
                        ({
                            type: "GET",
                            async: false,
                            url: weburl2 + 'queryType=SqlSp&sp=' + sp + '&paramlist=' + dgData.rows[dgIndex].ID,
                            success: function (html) {
                                //alert(html);
                                if (html.indexOf('Failed') !== -1) {
                                    $.messager.alert('Failed', 'Unable to remove', function (r2) { });
                                    reject();
                                }
                                else {

                                    $.messager.alert('Success', 'Successfully removed', function (r2) {

                                    });
                                    resolve();
                                }
                            }
                        });
                }
            });
        });
    },
    addnewEntry: function (sp, params) {
        return new Promise(function (resolve, reject) {
            $.ajax
                ({
                    type: "GET",
                    async: false,
                    url: weburl2 + 'queryType=SqlSp&sp=' + sp + '&paramlist=' + params,
                    success: function (html) {
                        if (html.indexOf('Failed') !== -1) {
                            $.messager.alert('Failed', html + ' . Please try again', function (r2) { });
                            reject();
                        }
                        else {

                            $.messager.alert('Success', 'Successfully added', function (r2) {

                            });
                            resolve();
                        }
                    }
                });
        });

    }
};

function JSONToCSVConvertorForCalculation(Query, ReportTitle) {

    var JSONData;
    var ShowLabel = true;

    $.ajax({
        url: Query,

        async: false,
        success: function (rec) {
            JSONData = rec;
            //alert(rec);
        }
    });
    JSONData = JSONData.replace(/null/g, '""');
    var arrData = typeof JSONData !== 'object' ? JSON.parse(JSONData, JSON.dateParser) : JSON.dateParser(JSONData);
    // var arrData = JSONData;
    var CSV = 'sep=,' + '\r\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";

        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {

            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);

        //append Label row with line break
        CSV += row + '\r\n';
    }

    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row2 = "";

        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index2 in arrData[i]) {
            row2 += arrData[i][index2] + ',';
        }

        row2.slice(0, row2.length - 1);

        //add a line break after each row
        CSV += row2 + '\r\n';
    }

    if (CSV === '') {
        alert("Invalid data");
        return;
    }

    //Generate a file name
    var fileName = "";
    //this will remove the blank-spaces from the title and replace it with an underscore
    fileName += ReportTitle.replace(/ /g, "_");

    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension    

    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");
    link.href = uri;

    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";

    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}

function JSONToCSVConvertor(Query, ReportTitle) {

    var JSONData;
    var ShowLabel = true;

    $.ajax({
        url: Query,

        async: false,
        success: function (rec) {
            JSONData = rec
            //   alert(rec);
        }
    });
    JSONData = JSONData.replace(/null/g, '""');
    var arrData = typeof JSONData !== 'object' ? JSON.parse(JSONData, JSON.dateParser) : JSON.dateParser(JSONData);
    // var arrData = JSONData;
    var CSV = 'sep=,' + '\r\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";

        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {

            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);

        //append Label row with line break
        CSV += row + '\r\n';
    }

    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row2 = "";

        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index2 in arrData[i]) {
            row2 += '="' + arrData[i][index2] + '",';
        }

        row2.slice(0, row2.length - 1);

        //add a line break after each row
        CSV += row2 + '\r\n';
    }

    if (CSV === '') {
        alert("Invalid data");
        return;
    }

    //Generate a file name
    var fileName = "";
    //this will remove the blank-spaces from the title and replace it with an underscore
    fileName += ReportTitle.replace(/ /g, "_");

    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension    

    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");
    link.href = uri;

    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";

    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}

function CSVGenerator(JSONArr, ReportTitle) {

    var ShowLabel = true;

    var arrData = JSONArr;

    var CSV = 'sep=,' + '\r\n';
    if (ShowLabel) {
        var row = "";
        for (var index in arrData[0]) {
            row += index + ',';
        }
        row = row.slice(0, -1);
        CSV += row + '\r\n';
    }
    for (var i = 0; i < arrData.length; i++) {
        var row2 = "";
        for (var index2 in arrData[i]) {
            row2 += '"' + arrData[i][index2] + '",';
        }
        row2.slice(0, row2.length - 1);
        CSV += row2 + '\r\n';
    }
    if (CSV === '') {
        alert("Invalid data");
        return;
    }
    var fileName;
    fileName = ReportTitle.replace(/ /g, "_");
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
    var link = document.createElement("a");
    link.href = uri;

    link.style = "visibility:hidden";
    link.download = fileName + ".csv";

    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}
if (window.JSON && !window.JSON.dateParser) {
    var reISO = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})?$/;
    var reMsAjax = /^\/Date\((d|-|.*)\)[\/|\\]$/;

    JSON.dateParser = function (key, value) {
        if (typeof value === 'string') {
            var a = reISO.exec(value);
            if (a)
                return value.replace("T", " ");
            a = reMsAjax.exec(value);
            if (a) {
                var b = a[1].split(/[-+,.]/);
                return new Date(b[0] ? +b[0] : 0 - +b[1]);
            }
        }
        return value;
    };

}
function setCookieFAMS(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookieFAMS(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

