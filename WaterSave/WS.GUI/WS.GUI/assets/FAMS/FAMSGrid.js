﻿function FAMSShieldGridBasic(URL) {
    $("#grid").shieldGrid({
        filtering: {
            enabled: true
        },
        rowHover: false,
        scrolling: true,
        paging: {
            pageSize: 40,
            pageLinksCount: 10
        },
        sorting: true,
        columns: LocalColumn,
        dataSource: {
            remote: {
                read: {
                    url: URL,
                    dataType: "json"
                }
            },
            aggregate: [
                { field: "Volume", aggregate: "sum" }
            ]

        },
        toolbar: [
            {
                buttons: [
                    {
                        commandName: "csv",
                        caption: '<span class="sui-sprite sui-grid-icon-export-excel"></span> <span class="sui-grid-button-text">Export to Excel</span>'
                    }
                ]
            }
        ],
        exportOptions: {
            proxy: "/filesaver/save",
            csv: {
                fileName: "FAMSExport" + (new Date()).getMilliseconds(),
                author: "FAMS",
                dataSource: {
                    remote: {
                        read: {
                            url: URL,
                            dataType: "json"
                        }
                    }
                },
                readDataSource: true
            }
        },
        selection: {
            type: "row"
        }
    });
}

function FAMSShieldGridExpand(URL) {
    $("#grid").shieldGrid({
        rowHover: false,
        scrolling: true,
        paging: {
            pageSize: 40,
            pageLinksCount: 10
        },
        sorting: true,
        columns: LocalColumn,
        dataSource: {
            remote: {
                read: {
                    url: URL,
                    dataType: "json"
                }
            },
            aggregate: [
                { field: "Volume", aggregate: "sum" }
            ]
        },
        toolbar: [
            {
                buttons: [
                    {
                        commandName: "csv",
                        caption: '<span class="sui-sprite sui-grid-icon-export-excel"></span> <span class="sui-grid-button-text">Export to Excel</span>'
                    }
                ]
            }
        ],
        exportOptions: {
            proxy: "/filesaver/save",
            csv: {
                fileName: "FAMSExport" + (new Date()).getMilliseconds(),
                author: "FAMS",
                dataSource: {
                    remote: {
                        read: {
                            url: URL,
                            dataType: "json"
                        }
                    }
                },
                readDataSource: true
            }
        },
        selection: {
            type: "row"
        },
        events: {
            detailCreated: detailCreated
        },
        filtering: {
            enabled: true
        },
    });
}

function detailCreated(e) {
     var mURLEx = "";
    if (iReportType == '1') {
        mURLEx = weburl + 'queryType=SqlSp&sp=QV_GetOverview_Detail2&paramlist=' + sessionStorage.getItem("Account_ID") + ',' + e.item.PID + ',' + sFromDate + ',' + sToDate + "&trick=" + (new Date()).getMilliseconds();
    }
    else if (iReportType == '2') {
        mURLEx = weburl + 'queryType=SqlSp&sp=QV_GetUsagePerStore_Detail&paramlist=' + sessionStorage.getItem("Account_ID") + ',' + e.item.PID + ',' + e.item.SID + ',' + sFromDate + ',' + sToDate + "&trick=" + (new Date()).getMilliseconds();
    }
    else if (iReportType == '3') {
        mURLEx = weburl + 'queryType=SqlSp&sp=QV_GetUsagePerEquipment_Detail&paramlist=' + sessionStorage.getItem("Account_ID") + ',' + e.item.PID + ',' + e.item.EID + ',' + sFromDate + ',' + sToDate + "&trick=" + (new Date()).getMilliseconds();
    }
    else if (iReportType == '4') {
        mURLEx = weburl + 'queryType=SqlSp&sp=QV_GetUsagePerAllocation_DetailRev2&paramlist=' + sessionStorage.getItem("Account_ID") + ',' + e.item.PID  + ',' + sFromDate + ',' + sToDate + "&trick=" + (new Date()).getMilliseconds();
    }
    else if (iReportType == '5') {
        mURLEx = weburl + 'queryType=SqlSp&sp=QV_GetUsagePerMasterEquipment_Detail&paramlist=' + sessionStorage.getItem("Account_ID") + ',' + e.item.PID + ',' + e.item.MID + ',' + sFromDate + ',' + sToDate + "&trick=" + (new Date()).getMilliseconds();
    }
    else if (iReportType == '6') {
        mURLEx = weburl + 'queryType=SqlSp&sp=QV_GetUsagePerCostCentre_Detail&paramlist=' + sessionStorage.getItem("Account_ID") + ',' + e.item.PID + ',' + e.item.CID + ',' + sFromDate + ',' + sToDate + "&trick=" + (new Date()).getMilliseconds();
    }
    else if (iReportType == '7') {
        mURLEx = weburl + 'queryType=SqlSp&sp=QV_GetTransfersPerEquipment_Detail&paramlist=' + sessionStorage.getItem("Account_ID") + ',' + e.item.PID + ',' + e.item.EID + ',' + sFromDate + ',' + sToDate + "&trick=" + (new Date()).getMilliseconds();
    }
    else if (iReportType == '9') {
        mURLEx = weburl + 'queryType=SqlSp&sp=QV_GetRecievingPerEquipment_Detail&paramlist=' + sessionStorage.getItem("Account_ID") + ',' + e.item.PID + ',' + e.item.EID + ',' + sFromDate + ',' + sToDate + "&trick=" + (new Date()).getMilliseconds();
    }
    else if (iReportType == '10'){
        mURLEx = weburl + 'queryType=SqlSp&sp=QV_GetUsagePerSite_Detail_Level1&paramlist=' + sessionStorage.getItem("Account_ID") + ',' + sessionStorage.getItem("User_ID") + ',' + e.item.SiteID + ',' + sFromDate + ',' + sToDate + "&trick=" + (new Date()).getMilliseconds();
    }

    $("<div/>")
        .appendTo(e.detailCell)
        .shieldGrid({
            dataSource: {
                remote: {
                    read: {
                        url: mURLEx,
                        dataType: "json"
                    }
                }
            },
            toolbar: [
                {
                    buttons: [
                        {
                            commandName: "csv",
                            caption: '<span class="sui-sprite sui-grid-icon-export-excel"></span> <span class="sui-grid-button-text">Export to Excel</span>'
                        }
                    ]
                }
            ],
            exportOptions: {
                proxy: "/filesaver/save",
                csv: {
                fileName: "FAMSExport" + (new Date()).getMilliseconds(),
                author: "FAMS",
                    dataSource: {
                        remote: {
                            read: {
                                url: mURLEx,
                                dataType: "json"
                            }
                        }
                    },
                    readDataSource: true
                }
            },
            rowHover: false,
            scrolling: true,
            paging: {
                pageSize: 40,
                pageLinksCount: 10
            },
            sorting: true,
            filtering: {
                enabled: true
            },
            columns: ExpandColumn
        });
}
