// markers style http://icon-park.com/
"use strict";
var markerColor = {};
(function(_sprite) {
  _sprite.shape = {
    //coords: [13.5, 0, 4, 3.75, 0, 13.5, 13.5, 43, 27, 13.5, 23, 3.75],
    coords: [
      14,
      44,
      8,
      30,
      0,
      19,
      0,
      7,
      7,
      0,
      20,
      0,
      28,
      7,
      28,
      19,
      20,
      30,
      14,
      44
    ],
    type: "poly"
  };
  var _image =
    " data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOAAAAAtCAMAAACAhZe5AAAC/VBMVEUAAACGhoOCgYGNfHiBgYGBgYGAgIBrfX+AgICBgYGAgICAgICAgICAgH+Af3+AgICAgICAgICAgICAgIAVeTxubm6AgICAgICAgIB/f4CAgICAgIA1NzbRThiAgIC2JwosLS0IGA0JcDcDSSI9aE/1PhGAgICAgIC0C1uEe3nrNwKxCVlmaGdfYmbkRQAFa7HXWgu/hx7sYQLYTQYgam8gjkdhanAHarESh0PePAABPnl7AUaSIVy4NCINebpbX2LbKwYFbbJpXWFPPUOXEgGeJhn+2gIRGhTmMAN9HiWqBVepEkkhUI7rsQAaSonqYAGKa2QSej7coATjWgBTZ4WdVEq/eAGHT2KJPUWWa1gNe7vZLwh0LD+QZV8oNi70xACpAFUNfz/vJAFYWllERERxD0k+RUGzCVofj0j/2QEgRXQ/Q0GpCFe1eR2uA1f5yAAhWzyegUwPXzSEFAmueSjpMAEUGRbYlwCCZmFEblfiGQMOP3TuuwHdnAVSUlL/0gCsAFWFWHCAgIDNzc3j4+Pf39/b29u/A1zn5+f09PTv7+/r6+vW1tb/QgXS0tL/GAA7nFBNTU2+GGG+FGAxlEt0dHT/JAL5+fkGXaq/CV3iPgC+Dl/8cQX3awTmRgD/DgD/OQT9/f0hi8UhhcD+xAEoi0b+MANFRUUdgkFVVVUCZjD/4AP/zQMbgL/0ZATtVQL/5gT/uAAXeLoCVqf+2gbpTQF6eHn/6wX/vQD7sQDxXQQScbYJbzYOdDkAUKC2AVnbNwBcXFwJY65pamplZWUMZ7D/1AMAS5fwpgC/GmOpAFU+Pj7imwFhYWHQNAK4xMQObLMAR4pDn1gAVigANxjsCQCaAFAgICDQjALfBwHIw8XFMAPGi6n2DQDQBAC5AQBOkMDTwr3eo4u/Xo5bn3Lw0U3n03Ppa1mOscrg0cVxosWqqqqUuKB5rIrSz7iamprk1JXii4DglG3nf0PwWD/dsKeKiorARH+LY2f20zDrcSbOtbu+K3Dmh1WRgGGpsvpDAAAAgnRSTlMABhQLJRtHEPosNe+Radg/YbajUv7+v5pwfufg/hfJ/v7+/f5B/dF19YPn3diYlUoz8uNGJN6/noBf/v737eB+fHhsVfzr29rSqXo//fLYw66jnHn7+/j1ysnGqY6MiXldXfnt5N/Lw8Owq6qplZM5+vjb08W7rmZI/O3n2MmhWC7JTKyY4gAACu9JREFUaN58l11IU2EcxlMJqctqF114p7DmcjKz1Y1pgYQSol6lF9oHpCR2JUH0hZvmV2Y2Z2FFpUbiRyFqZUWYJQjFQuhGFszNTfP7K0VT6H2f/3u2855Zz256n9/p/M9v73mF7VASlhB30hATYyhJjt+hDfiZzAK73Z5YcOTQ9txkyens7EzKSfkHv1GY2tPenlqYdykU0vwMc1eXOSP7H/Pziy/o9frYC2k3t+fRB406nc54+Ni28yOSDbeVxJQkhP73I4l1IyL2guhQnpL0TElzjimU56W+VvKqMFQxItv8UMmjjIRQnhZbXz+M1N+7kB56/2PGvlJKX8Ph0OeLh15QMTdC5tFMT/WxZ0bK3AS9oKJFwy9BT6UYoZkPvaBinIanx5KdolikfT5jnzoNBzX8aMxtTU5GqfkZe50miYckv85nmuRI/EY7vFSGqVHS/K6HmmRIPF9fr0msxI/r+jQx7vy/HwxlP61huOzXrPkkRar9XmkDQ8nvUXD/+L/NKp5/T7bTGp7QNYTEqHq+ePhpUxIW2H/4aVMATn7N2yQnwC/1QEmTQnDM73q0TTIC/KY+1I8dxADfDT9tDgd4xMknIitLi4sTMcoqWfCwRLvI+OzMzOyIsjqi8CSh1Omempycau4UqxSFp7aLrPv9fl+PsspT5puFUtfykseztaEYZis89h6lfs3lcnk368UyTbm/USjpSuenp+cbdGJ1TBFMVvTGbDZbWXmNRygaoogfUfScxGfqaN0aTjylk+K2Ep9spnV3JPG8HqH3i/iCWHeI+2eLDVwW3CMEzYKnKXri/q5NKvSc4wBSSgWfbqD1oHi+MAP5TNgqWPgVY8IwWdrA2QB3jogtFBtIPlNBLgxTpA30BfgcGfbkETd3IVvB+RvUZBMXG+gN8FFhmEbcSD6rQU6GyhYmPKf9s1VUV1VVVVewK/6QoQE8upX2T8WdJJgIbupmm8X2T81Zwz5JdAKHenh8CochcpXmk82yej5VZvD0Ji7T5FXzYQjG0vMNIqXq+TpURhKMe478qqiqrGysrKq2ldXULlIXjze0FbGq+Sx1hzi3dCO1aj5FHXgWZIak+/vRdcRjPmT6Jb5EHXgxvaDS/V3omm5yfhAynyU+T90hCJ6Ey4Stuqrx6VN2BfsKasdiUB7lvAAu4xJ32lGe4TwHLm6Zk6CJ86u0gRKfI+nLnGf082zJ87tQYv6FJh6vxEeHUeZzbvzMsyrzQZQnIGiAi8dWXckueEoXWFdQ4hAmwmVG5iMocQiT4DIpczdKHMJTQzwLMl9HiUNohotm/gZKHEI9XFwyX0OJQ3gOLtMyL0VJh/AsXH7TBY3iggmUcZy/gItT5uNBwXeIhrtRWlSCMvehzOL8JVw085dRYv5HuPyRuRdlMecD33g081dRHiRBBwsTZO9wY2NlZTUJOnhIEHHKfBylSlDismAHz4LMfShJENHMX0aJ+XoSlHmooMQlQQPTczg8ZewlrmRntMJWXmO1rqCkVxQuMzIfCQomwWVS5iSYgjNIgjJfR4kzaIaLZv4GSryisfSKSlx6RQd4pmVeipJe0RJs1kR5Gfs7W804/wJGn6BM4DwTLuMyb0UZzbnlO49b5u9Qmji/BhefxOeGeNeC+2fAZUviY/0owYse8Hjl+w+jTOf8OlxWZf4NZTQEj8Ll+S92BUtZGf8CFtGdjeD8zAvEquaz1EVyngKXdxKfQvcF/HILDKX7+1GdisD8XshIfAlVL3g+XIal+7vQ6cGPD7Sxz4DE5wcGWHcOfEfUWdrCmvJyRstraq3WsRhUueCR+5BxFXe2osok/gVxq3k3Kgvd/1QLj0/F5zpQZRHvRbbU8/tRxYGH62kLVXx0E1URzW9DVtX8M6rrOyhxjjvs41isrWGp5fdnJ5A1Z9MjwvCOkuFsgDtHqIkmbiHDqSBvRvHJRDyrBfEH+Nw6irfxxOPIcCk4fwPFY8GLHyCuAB9dQ/FRPN91MpwP8gZq8HwsUVfuIBNjVuR3DK1zd+1kV2ALkXEn8ZlWWmcq/BPFLfhkN60t4NhCxDdHfKGD1lkKN5Dhspjv6ad1rsL1ZOgdJe7apHWR4OHnyadU8OlBWl8Exyn4ybaQxTGx+NuzuOIgvysH9u7eSafwA+UFfi7V7aPVvv0KN90noy/0c0n43g/wy2+5zxum6F/w+9dbKF9xf8x/TOnlP5eWNnppZQjw/I+3kGGvi2XtAa30AX7iLqUNP5d0bWJF85HcO6H5cXrP/r27wukl/RCa98Ygt9zfJoeDPOtNaP52Xi6vTURRHCbNy8REa4vv4HSs+KTooLjIJPhKqNEQukjTimk3Nl20BvpCLMWF3bjuQhAXugy6k4JQkDZgoSENhdKYNpQ2/4Yr8Z7Jncy9cx9ifjOBwjcz53w5d25JiXj+809sPhL85Ts2a28tPrnECe4P/+Rl/A4Gz1/oOuNxOYF7Zy4ymSd5ivVLE9z5hvFbmSb5Y9ZvkOQ3WL8hsv44o1dYtzh+DT9bB/KLHjl6/lSg2+d1AL8z88OWeZov2/SWkwSH1/AbnRWN5tc/2hKlOWMYoeuPGVLmgf78QnDI01+fqSiLi0fOXe4K+vGML32l/aZs/ObqMpkPSZo7bpVoQc12/22Q+mR9tmz86VWbn72/AsqS9VmycYf3/gHpV/+OLjh6rOuM2aB39CuV93b+gBJcZe7Pr5B+lUU7v0/Nr8jc/2SN9Cuz/RWoWBwLuo9HCb+DQaOB5jeA+QzS+mGe8yxPkX5plr9ZITLN8sfghc8ip/4N6gVk+Riht7luckJwpE4tUBjxhdOdvtYD+nas+U3x+KqVJIePlCw/jceLRWTWPPd5vEwtULb+ppUCybGg52z0wEz9d3MFnTiLdqEWn2n57fzk8VTLbyPN41rLr/SHx6PFVrj1I2tmylw+1vILnSS5OcErA/VfOApw2IQ6/W5Hi9/dMTPF5xvkAFk+UCnhaHy+ZfpF+bxsCkb49UNmCiTHgh3+4Oko9kMDPIL8T51G/0a8DotPYb/edT5PbeCk+VzDfpU/fB7dwhHUj5gDFPAx7Nfzk+J4F3V1X3t2WDeiIP1zx04g7usg+d1eHBHHfkkBf1ZBcujYE/H9pp+o/ttyMxFR/Z6eEJwhiuM43f7OgGL4HQ6i8V44EQgyHPt9EfEkHqCIaxUj0yLeHOG+sH6kKTgk4kgPwvSHvwLPmeFDSAPG23W287jLzser1d5qtfpexCe2jQjvnzP89oR8eB+iiHkN/Gri/gy/3e92jlexr3ukAYLxc2j5Xun2u7x2fq8KCQl5wPBLCvm1PRDUhHxEAcG4+P4aJCLubxeyyXB8BRpyHAY4dMwYr9vJ8n4QfCHmKRBMi7m2hzIt5nEYoKR+DgRl/YEg2x++wOk+nmigIA7j5fFJEJRwWKNhCc+DoIQnFBQJzyI/XdYfCNKcHvJD5BcHjsfLcFVVe2Q8vL29IOFB5KfJ7kd+0vp6rZaT9re7GzI5G6fLE280hgNBMe9X1XEZXwiHUzKO1uicjMcVRVo/p+tt9odXcaIRGwh6xPyVqvbJ+EQ4LOVzmYz0+QklJue63tdWf3ir9WVjMcQ7hHxUVaX8Xjgs5flMBnjb9bO63mZ/eMavY7Men4RfUvul3BNOSfnrzBzi0vpyrufa7A+/pq7ZhMvtFGHE+0flfGFCzjN5OZ/Nynku22Z/eKftSDxC+6uEv/oHn/gHz98G3n79yf/v7y/yPn2dakxIzAAAAABJRU5ErkJggg==";
  var _color = ["red", "green", "blue", "yellow", "orange", "purple", "black"];
  var _w = 32;
  var _h = 45;
  var _inc = _w;
  var _ax = 14;
  var _ay = _h + 1;
  _color.forEach(function(color, ind) {
    _sprite[color] = {
      url: _image,
      size: {
        width: _w,
        height: _h
      },
      anchor: {
        x: _ax,
        y: _ay
      },
      origin: {
        x: ind * _inc,
        y: 0
      },
      scaleSize: {
        width: _w,
        height: _h
      },
      labelOrigin: {
        x: _ax,
        y: 0
      }
    };
  });
  return _sprite;
})(markerColor);
